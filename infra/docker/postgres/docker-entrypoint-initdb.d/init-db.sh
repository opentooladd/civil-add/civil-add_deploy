#!/bin/bash
set -e

entrypoint_path="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

## $1 : db_usr
## $2 : db_pwd
## $3 : db_name
function init_db() {

  psql \
    -U "$POSTGRES_USER" \
    -d "$POSTGRES_DB" \
    -f "${entrypoint_path}/sql/create-user.sql" \
    -v db_usr="$1" \
    -v db_pwd="$2"

  psql \
    -U "$POSTGRES_USER" \
    -d "$POSTGRES_DB" \
    -f "${entrypoint_path}/sql/init-db.sql" \
    -v db_usr="$1" \
    -v db_name="$3"

}

init_db "$POSTGRES_KEYCLOAK_USER" "$POSTGRES_KEYCLOAK_PASSWORD" "$POSTGRES_KEYCLOAK_DB"
init_db "$POSTGRES_CVAD_BACK_USER" "$POSTGRES_CVAD_BACK_PASSWORD" "$POSTGRES_CVAD_BACK_DB"
