#!/bin/bash
set -e

pg_dump -U "$POSTGRES_ADMIN_USR" "$POSTGRES_KEYCLOAK_DB" > /docker/env/data/local/keycloak_dump.sql
pg_dump -U "$POSTGRES_ADMIN_USR" "$POSTGRES_CVAD_BACK_DB" > /docker/env/data/local/civil-add_dump.sql