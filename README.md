# Civil-Add Deploy

This project is a temporary solution to solve consistant deployment management
of civil-add front and back projects.

## first install

1. ensure hostname target your machine ip.
    For this project, you need two hostname, one for civil-add,
    one for keycloak.

    > :point_up: for local install you want to edit `/etc/hosts` file.

    > For exemple : run `sudo editor /etc/hosts` and add lines
    > ```
    > 127.0.0.1       civil-add.open.tool-add.local
    > 127.0.0.1       oauth.open.tool-add.local
    > ```
21. copy all `infra/env/*.env.template` to `infra/env/*.env`
    > Useful command :
    >
    > `find infra/env/*.env.template | sed 's/\.template//g' | xargs -I {} sh -c "cp {}.template {}"`

      then edit them following given `{{ explanations and exemples }}` in the .env.templates, careful about not leaving blanck spaces

    > :point_up: it is normal if you don't know how to handle `CVAD_BACK_KEYCLOAK_CLIENT_*` variables.

    > :warning: Postgres superuser and password cannot begin with `pg_` prefix.
    > postgres-civil-add    | initdb: error: superuser name "pg_user" is disallowed; role names cannot begin with "pg_"


21. Update submodules :
    > `make infra-git-init`
    >
    > `make infra-git-update`

    > **TODO** : review this command
21. \[UNDER WORK] generate local ssl certificats
    > `make proxy-gen-ssl`
    >
    > You may give some information there

    > **TODO** : work on generic commands depending on `CVAD_DEPLOY_ENV` to generate local
    > certificates or use [Let's Encrypt](https://letsencrypt.org/fr/)
21. Download and build your project
    > `make infra-init`
21. Setup your Keycloak server
    1. Connect to admin console : https://{PROXY_KEYCLOAK_HOSTNAME}/
        > :point_up: you can connect as administrator using `KEYCLOAK_USER`/`KEYCLOAK_PASSWORD`
    21. `Clients` >> `{CVAD_BACK_KEYCLOAK_CLIENT_ID}` >> `Credentials`
        > :point_up: default value of `CVAD_BACK_KEYCLOAK_CLIENT_ID` is `civil-add_back`
    21. Regenerate the `ClientSecret` by clicking on the button, and copy the value
        > :warning: you will have access to the value only once ! be careful to copy it.
        Else, regenerate it.
    21. Edit your `infra/env/cvad_back.env` file with proper `CVAD_BACK_KEYCLOAK_CLIENT_ID`/`CVAD_BACK_KEYCLOAK_CLIENT_SECRET` values (the one you copied)
21. Create a Civil-Add user on your Keycloak server
    1. Connect to admin console : https://{PROXY_KEYCLOAK_HOSTNAME}/
    21. `Users` >> `Add user`
        * `login` : choose your {user login}
        * `User Enabled` : On
        * `Email Verified` : On
        * >> `Save`
    21. `Users` >> `Edit` (for {user login} user you just created)
    21. `Credentials` to add a password
21. Now you edited `infra/env/cvad_back.env`, you need to regenerate civil-add back-end configuration
    > `make cvad_back-gen-conf`
21. Now you updated configuration, reload civil-add back-end.
    > `make cvad_back-reload`
21. You should be able to connect to https://{PROXY_CVAD_HOSTNAME}/{PROXY_CVAD_API_PATH_PREFIX}/auth/login
    using the user you created in Keycloak few lines above.

> :point_up: **TODO** we still have some problems to solve :
> * logout problems. The workaround is to call https://{PROXY_CVAD_HOSTNAME}/{PROXY_CVAD_API_PATH_PREFIX}/auth/logout
postgres-civil-add, and to close your session in Keycloak.
> * `initdb: error: superuser name "pg_user" is disallowed;` role names cannot begin with "pg_"

## Usage

### Update *.env files

1. Change your settings
21. Regenerate your configurations : `make infra-gen-conf`
21. Reload services : `make infra-reload`

### Development

...

### Log and debug

...

## Project structure and description

**TODO**

...


## Tools and useful commands

### Clean Docker environment

You may need to clean all your local Docker environment :
```sh
docker stop $(docker ps -a -q)
docker rm -v $(docker ps -a -q)
docker volume rm $(docker volume ls -qf dangling=true)
docker rmi $(docker images -a -q) -f
```

## Documentation

Setup
* [How to set up an easy and secure reverse proxy with Docker, Nginx & Letsencrypt](https://www.freecodecamp.org/news/docker-nginx-letsencrypt-easy-secure-reverse-proxy-40165ba3aee2/)
* [Running a Docker container as a non-root user](https://medium.com/redbubble/running-a-docker-container-as-a-non-root-user-7d2e00f8ee15)

Stack
* [Keycloak Docker image](https://hub.docker.com/r/jboss/keycloak/)

Use-full beginner links :
* https://cloudmaker.dev/
* https://doc.rust-lang.org/stable/rust-by-example/index.html


## Todo

* documentation : describe usage / structure
* deployment
    * manage left blanck spaces in .env
    * develop health check script for deployment (hostname ips, databases access/install, containers, build, ...)
    * ssl : self signed in local / let's encrypt for server
        https://www.home-assistant.io/docs/ecosystem/certificates/tls_self_signed_certificate/
        https://letsencrypt.org/docs/certificates-for-localhost/
    * define proper release process
        * multi project (front / back / lib / ...)
        * environments : local / test/demo / prod
        * data (backup / migrations / import)
        * settings (security and usage)
        * docker images tag naming
            https://www.poftut.com/how-to-list-commit-history-with-git-log-command-with-examples/
            https://stackoverflow.com/questions/9059335/how-to-get-the-parents-of-a-merge-commit-in-git
            build docker image tag value from current branch but adapt to docker characters constraints (ex: feature/CVAD-4718/j'ai fait ma modif)
            prepare for [semantic flow]() ? get tag name to define versions
    * Create a docker release image for back-end
        https://gist.github.com/belst/ff36c5f3883f7bf9b06c379d0a7bed9e
        https://render.com/docs/deploy-rocket-rust
        https://medium.com/@steadylearner/how-to-deploy-rust-web-application-8c0e81394bd5
        https://docs.docker.com/develop/develop-images/multistage-build/
        * Deploy Rocket server
            > * https://gist.github.com/belst/ff36c5f3883f7bf9b06c379d0a7bed9e
            > * https://render.com/docs/deploy-rocket-rust
            > * https://medium.com/@steadylearner/how-to-deploy-rust-web-application-8c0e81394bd5
    * Create a docker release image for front-end
        https://docs.docker.com/develop/develop-images/multistage-build/
    * Use alpine for production images
    * diesel : review and document migration process (schema.rs in src)
* keycloak
    * manage real import/export into keycloak properly
        https://hub.docker.com/r/jboss/keycloak/
        * add environment management (default values for local / custom values for demo / pre-prod / prod)
    * user creation by email in production
* civil-add front-end
    * manage npm packages vulnerabilities
    * update cvad_deploy project dependency
* enhance *.env usage to allow `$` characters (passwords)
    `$$` ?
* add integration tests
