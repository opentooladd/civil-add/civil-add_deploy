# Useful to ensure files produced in volumes over docker-compose exec
# commands are not "root privileged" files.
export CURRENT_UID=$(id -u):$(id -g)

ifndef DOCKER_COMPOSE
DOCKER_COMPOSE := docker-compose \
	-f infra/docker/cvad_front.docker-compose.yaml \
	-f infra/docker/proxy.docker-compose.yaml \
	-f infra/docker/cvad_back.docker-compose.yaml \
	-f infra/docker/keycloak.docker-compose.yaml \
	-f infra/docker/postgres.docker-compose.yaml
endif

# Self
export CVAD_DEPLOY_ENV_PATH=$(shell realpath ./infra/env)
include ${CVAD_DEPLOY_ENV_PATH}/cvad_deploy.env

# Proxy
include ${CVAD_DEPLOY_ENV_PATH}/proxy.env
PROXY_KEYCLOAK_CONF_PATH=infra/docker/proxy/nginx/conf.d/sites-available/keycloak.conf
PROXY_CVAD_CONF_PATH=infra/docker/proxy/nginx/conf.d/sites-available/civil-add.conf

# Postgres
# TODO : find a way to manage dependencies properly
include ${CVAD_DEPLOY_ENV_PATH}/postgres.env
export POSTGRES_KEYCLOAK_USER	# used for keycloak docker-compose
export POSTGRES_KEYCLOAK_PASSWORD	# used for keycloak docker-compose
export POSTGRES_KEYCLOAK_DB		# used for keycloak docker-compose

# Keycloak
# TODO : create realm for prod ? => remove credentials ? force client key regenerate ?
# TODO : create an export script / command
export KEYCLOAK_LOCAL_REALM=$(shell realpath ./data/${CVAD_DEPLOY_ENV}/keycloak.realm.json)

# CVAD Back
include ${CVAD_DEPLOY_ENV_PATH}/cvad_back.env
CVAD_BACK_LOCAL_VOLUME=infra/docker/cvad_back
export CVAD_PATH=/var/civil-add
export CVAD_BACK_SRC=$(shell realpath ./src/cvad_back)
export CVAD_BACK_DATABASE_URL=postgres://${POSTGRES_CVAD_BACK_USER}:${POSTGRES_CVAD_BACK_PASSWORD}@postgres-civil-add:5432/${POSTGRES_CVAD_BACK_DB}

# CVAD Front
#include ${CVAD_DEPLOY_ENV_PATH}/cvad_front.env
CVAD_FRONT_LOCAL_VOLUME=infra/docker/cvad_front
CVAD_FRONT_CRATE_NAME=civil-add_front
export CVAD_FRONT_SRC=$(shell realpath ./src/cvad_front)

# TODO : how to transmit environment variables to containers ?

## Proxy
#########

proxy-gen-conf:
	@touch ${PROXY_KEYCLOAK_CONF_PATH}
	@echo "upstream ${PROXY_KEYCLOAK_HOSTNAME} { server keycloak-civil-add:8080; }" > ${PROXY_KEYCLOAK_CONF_PATH}
	@echo 'server {' >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo "		listen  443 ssl; server_name ${PROXY_KEYCLOAK_HOSTNAME};" >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo '		include common.conf;' >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo '		include allow_cors_headers.conf;' >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo '		include ssl.conf;' >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo '		location / {' >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo "			proxy_pass http://${PROXY_KEYCLOAK_HOSTNAME};" >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo "			include common_location.conf;" >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo "		}" >> ${PROXY_KEYCLOAK_CONF_PATH}
	@echo '}' >> ${PROXY_KEYCLOAK_CONF_PATH}
	@############################################################################
	@touch ${PROXY_CVAD_CONF_PATH}
	@echo "upstream ${PROXY_CVAD_HOSTNAME}_back { server cvad_back:80; }" > ${PROXY_CVAD_CONF_PATH}
	@echo "upstream ${PROXY_CVAD_HOSTNAME}_front { server cvad_front:8080; }" >> ${PROXY_CVAD_CONF_PATH}
	@echo 'server {' >> ${PROXY_CVAD_CONF_PATH}
	@echo "		listen  443 ssl; server_name ${PROXY_CVAD_HOSTNAME};" >> ${PROXY_CVAD_CONF_PATH}
	@echo '		include common.conf;' >> ${PROXY_CVAD_CONF_PATH}
	@echo '		include ssl.conf;' >> ${PROXY_CVAD_CONF_PATH}
	@echo "		location /${PROXY_CVAD_API_PATH_PREFIX} {" >> ${PROXY_CVAD_CONF_PATH}
	@echo "			return 302 /${PROXY_CVAD_API_PATH_PREFIX}/;" >> ${PROXY_CVAD_CONF_PATH}
	@echo '		}' >> ${PROXY_CVAD_CONF_PATH}
	@echo "		location /${PROXY_CVAD_API_PATH_PREFIX}/ {" >> ${PROXY_CVAD_CONF_PATH}
	@echo "			proxy_pass http://${PROXY_CVAD_HOSTNAME}_back/;" >> ${PROXY_CVAD_CONF_PATH}
	@echo "			proxy_redirect / /${PROXY_CVAD_API_PATH_PREFIX}/;" >> ${PROXY_CVAD_CONF_PATH}
	@echo '			include common_location.conf;' >> ${PROXY_CVAD_CONF_PATH}
	@echo "		}" >> ${PROXY_CVAD_CONF_PATH}
	@echo '		location / {' >> ${PROXY_CVAD_CONF_PATH}
	@echo "			proxy_pass http://${PROXY_CVAD_HOSTNAME}_front/;" >> ${PROXY_CVAD_CONF_PATH}
	@echo '			include common_location.conf;' >> ${PROXY_CVAD_CONF_PATH}
	@echo '		}' >> ${PROXY_CVAD_CONF_PATH}
	@echo '}' >> ${PROXY_CVAD_CONF_PATH}

# TODO : generic ssl configuration (self-signed vs lets-encrypt)
# For better security, you can use 4096 instead of 1024
proxy-gen-ssl:
	cd infra/docker/proxy/certs/; openssl req -sha256 -addext "subjectAltName = IP:127.0.0.1" -newkey rsa:1024 -nodes -keyout privkey.pem -x509 -days 730 -out fullchain.pem
	cd infra/docker/proxy/nginx/; openssl dhparam -out dhparams.pem 1024

proxy-reload:
	${DOCKER_COMPOSE} exec proxy-civil-add sh -c 'nginx -s reload'

## Postgres
######################
postgres-down:
	${DOCKER_COMPOSE} down --volumes postgres-civil-add


## Civil-Add back-end
######################

cvad_back-gen-conf:
	@mkdir -p ${CVAD_BACK_LOCAL_VOLUME}/configuration
	@touch ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo '[civil_add]' > ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "front_base_path='https://${PROXY_CVAD_HOSTNAME}'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "api_base_path='https://${PROXY_CVAD_HOSTNAME}/${PROXY_CVAD_API_PATH_PREFIX}'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo '[database]' >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "url='${CVAD_BACK_DATABASE_URL}'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo '[keycloak]' >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "provider_token_uri='http://keycloak-civil-add:8080/auth/realms/civil-add/protocol/openid-connect/token'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "provider_auth_uri='https://${PROXY_KEYCLOAK_HOSTNAME}/auth/realms/civil-add/protocol/openid-connect/auth'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "client_id='${CVAD_BACK_KEYCLOAK_CLIENT_ID}'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "client_secret='${CVAD_BACK_KEYCLOAK_CLIENT_SECRET}'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml
	@echo "redirect_uri='https://${PROXY_CVAD_HOSTNAME}/${PROXY_CVAD_API_PATH_PREFIX}/auth/redirect'" >> ${CVAD_BACK_LOCAL_VOLUME}/configuration/civil-add.toml

cvad_back-build:
	@${DOCKER_COMPOSE} exec cvad_back sh -c 'cargo build'
	@${DOCKER_COMPOSE} exec cvad_back sh -c "diesel --database-url ${CVAD_BACK_DATABASE_URL} setup"

# TODO : review / define
cvad_back-reset-db:
	#${DOCKER_COMPOSE} exec cvad_back sh -c "diesel --database-url ${CVAD_BACK_DATABASE_URL} migration revert"
	#${DOCKER_COMPOSE} exec cvad_back sh -c "diesel --database-url ${CVAD_BACK_DATABASE_URL} migration redo"
	${DOCKER_COMPOSE} exec cvad_back sh -c "diesel --database-url ${CVAD_BACK_DATABASE_URL} migration run"

cvad_back-up:
	${DOCKER_COMPOSE} up -d cvad_back

cvad_back-run: cvad_back-up
	${DOCKER_COMPOSE} exec -d cvad_back sh -c 'cargo run'

cvad_back-stop:
	${DOCKER_COMPOSE} stop cvad_back

cvad_back-reload: cvad_back-stop cvad_back-run

cvad_back-watch: cvad_back-up
	${DOCKER_COMPOSE} exec cvad_back sh -c 'cargo watch -x run'

#cvad_back-test:
#	${DOCKER_COMPOSE} exec cvad_back sh -c 'cargo test --package civil_add user_service'
	#${DOCKER_COMPOSE} exec cvad_back sh -c 'cargo test civil_add::services::user_service'
	#${DOCKER_COMPOSE} exec cvad_back sh -c 'cargo test'



## Civil-Add front-end
#######################

cvad_front-gen-conf:
	@touch ${CVAD_FRONT_LOCAL_VOLUME}/.env
	@echo "CVAD_API_HOST=https://${PROXY_CVAD_HOSTNAME}/${PROXY_CVAD_API_PATH_PREFIX}" > ${CVAD_FRONT_LOCAL_VOLUME}/.env

cvad_front-build:
	${DOCKER_COMPOSE} exec cvad_front sh -c 'cargo build'
	${DOCKER_COMPOSE} exec cvad_front sh -c 'wasm-pack build'
	${DOCKER_COMPOSE} exec cvad_front sh -c "cd www && npm install && npm run build-crate -- ${CVAD_FRONT_CRATE_NAME} && npm install"
	${DOCKER_COMPOSE} exec cvad_front sh -c './scripts/build_pages.sh'
	${DOCKER_COMPOSE} exec cvad_front sh -c 'cp -r ./src/css/icons ./target/web/icons'

cvad_front-run:
	${DOCKER_COMPOSE} exec -d cvad_front sh -c 'cd www && npm run test-server'


## Infra
#########

infra-gen-conf: proxy-gen-conf cvad_back-gen-conf cvad_front-gen-conf

infra-reload: proxy-reload cvad_back-reload cvad_front-build

infra-run:
	${DOCKER_COMPOSE} up -d --remove-orphans
	make cvad_front-run
	make cvad_back-run

# stop containers and processes
infra-stop:
	${DOCKER_COMPOSE} stop

# we dissociate 'env' from 'infra' to avoid deployment "mistakes"; env is reserved for "risky" operations

env-init: infra-gen-conf
	${DOCKER_COMPOSE} up -d
	make cvad_front-build
	make cvad_back-build
	# TODO : prepare infra/env/*.env files


# infra-down is used to clean project and docker environment
env-down: clean-all
	${DOCKER_COMPOSE} down --volumes

infra-git-init:
	git pull
	git submodule update --init

infra-git-update:
	git pull
	git submodule update --remote


## Diagnostic
##############

log-keycloak:
	${DOCKER_COMPOSE} logs -f keycloak-civil-add

log-postgres:
	${DOCKER_COMPOSE} logs -f postgres-civil-add

log-proxy:
	${DOCKER_COMPOSE} logs -f proxy-civil-add

log-cvad_front:
	${DOCKER_COMPOSE} logs -f cvad_front

log-cvad_back:
	${DOCKER_COMPOSE} logs -f cvad_back

status-git:
	git config -l | cat
	git status


## Clean
#########

clean-proxy:
	- @find infra/docker/proxy/nginx/conf.d/sites-available/* ! -name .gitignore | sort -nr | xargs rm -r
	# TODO : clean ssl

clean-cvad_back:
	- @${DOCKER_COMPOSE} exec cvad_back sh -c 'find ${CVAD_PATH}/* ! -name .gitignore | sort -nr | xargs rm -r'
	- @${DOCKER_COMPOSE} exec cvad_back sh -c 'cargo clean'
	@#${DOCKER_COMPOSE} exec -u ${CURRENT_UID} cvad_back sh -c 'rm -r public/doc public/coverage'

clean-cvad_front:
	- @${DOCKER_COMPOSE} exec cvad_front sh -c 'rm -r pkg'
	- rm infra/docker/cvad_front/.env
	- @${DOCKER_COMPOSE} exec cvad_front sh -c "find -path '*node_modules*' -o -path '*dist*' | sort -nr | xargs rm -r"
	- @${DOCKER_COMPOSE} exec cvad_front sh -c 'cargo clean'
	# TODO : clean `pkg` and `target` in src/pages

clean-all: clean-proxy clean-cvad_front clean-cvad_back
	# TODO : rm infra/env/*.env


## Shell
#########

shell-proxy:
	${DOCKER_COMPOSE} exec proxy-civil-add sh -c '/bin/ash'

shell-cvad_back:
	${DOCKER_COMPOSE} exec cvad_back sh -c '/bin/bash'

shell-cvad_front:
	${DOCKER_COMPOSE} exec cvad_front sh -c '/bin/bash'

shell-keycloak:
	${DOCKER_COMPOSE} exec keycloak-civil-add sh -c '/bin/bash'

shell-postgres:
	${DOCKER_COMPOSE} exec postgres-civil-add sh -c '/bin/bash'

shell-psql-cvad:
	${DOCKER_COMPOSE} exec postgres-civil-add sh -c 'psql -U "$(POSTGRES_CVAD_BACK_USER)" -d "$(POSTGRES_CVAD_BACK_DB)"'

shell-psql-keycloak:
	${DOCKER_COMPOSE} exec postgres-civil-add sh -c 'psql -U "$(POSTGRES_KEYCLOAK_USER)" -d "$(POSTGRES_KEYCLOAK_DB)"'
